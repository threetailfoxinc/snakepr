// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakePawn.generated.h"

class UCameraComponent;
class ASnakeBase;
UCLASS()
class SNAKE_API ASnakePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASnakePawn();
	UPROPERTY()
		float MinX = -500.f;
	float MaxX = 500.f;
	float MinY = -1250.f;
	float MaxY = 1250.f;
	float DelayTime = 3.f;
	float BuferTime = 0;
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase>SnakeActorClass;
	UPROPERTY()
		class AFood* FoodActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>FoodActorClass;
	

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();
	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	void AddFoodRandom();

	void AddBonusRandom();

};
